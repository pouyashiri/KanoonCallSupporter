package com.example.rekabi.counsellorkanoon;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;

public class GetBookListAdapter extends BaseAdapter{

    Context context;
    SharedPreferences settings;

    int pos,isChecked=0,price=0;
    String reqId;
    ArrayList<String> BookName;
    ArrayList<String> BookPrice;
    ArrayList<String> BookId;
    private TextView sumPrice;
    ArrayList<Integer> addCheckBoxValue;
    ArrayList<Boolean> turns = new ArrayList<>();
    ArrayList<Integer> getPic = new ArrayList<>();

    LayoutInflater inflater;


    public GetBookListAdapter(Context context, String reqId,ArrayList<String> bookId, ArrayList<String> bookName, ArrayList<String> bookPrice,ArrayList<Integer> addCheckBoxValue){
        this.context = context;
        this.reqId = reqId;
        this.BookName = bookName;
        this.BookPrice = bookPrice;
        this.BookId = bookId;
        this.addCheckBoxValue = addCheckBoxValue;
        settings = context.getSharedPreferences("Supporter", 0);

        getPic.addAll(Collections.nCopies(bookName.size(),R.drawable.nobook1));
//        turns.addAll(Collections.nCopies(bookName.size(),true));
        for(int i=0;i<addCheckBoxValue.size();i++){
          if(addCheckBoxValue.get(i)==1){
              turns.add(true);
          }else if(addCheckBoxValue.get(i)==0){
              turns.add(false);
          }
        }

        inflater = LayoutInflater.from(context);
    }



    @Override
    public int getCount() {
        return BookName.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        final ViewHolder viewHolder;


//        if(convertView == null){
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_book,parent,false);
            viewHolder.rowBook = convertView.findViewById(R.id.rowBook);
            viewHolder.getbook = convertView.findViewById(R.id.getbook);
            viewHolder.bookName = convertView.findViewById(R.id.bookName);
            viewHolder.bookPrice = convertView.findViewById(R.id.bookPrice);

//            viewHolder.getbook.setImageDrawable(convertView.getResources().getDrawable(R.drawable.nobook));

//            convertView.setTag(viewHolder);
//        }else{
//            viewHolder = (ViewHolder) convertView.getTag();
//        }

//        viewHolder.getbook.setTag(position);
//        viewHolder.bookName.setTag(position);
//        viewHolder.bookPrice.setTag(position);
//        viewHolder.getbook.setTag(position);



        viewHolder.bookName.setText(BookName.get(position));
        viewHolder.bookPrice.setText(BookPrice.get(position)+" ت");
        viewHolder.getbook.setImageResource(getPic.get(position));
        final Typeface tvFont = Typeface.createFromAsset(context.getAssets(), "fonts/B Traffic Bold_p30download.com.ttf");
        viewHolder.bookName.setTypeface(tvFont);
        viewHolder.bookPrice.setTypeface(tvFont);
        if (turns.get(position)){
//            turns.set(position,false);
            viewHolder.getbook.setImageResource(R.drawable.okbook1);
            isChecked = 1;
            addCheckBoxValue.set(position,isChecked);
        } else {
//            turns.set(position,true);
            viewHolder.getbook.setImageResource(R.drawable.nobook1);
            isChecked = 0;
            addCheckBoxValue.set(position,isChecked);
        }
 

        viewHolder.rowBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("item clicked ","");

                if(!turns.get(position)){
                    turns.set(position,true);
                    getPic.set(position,R.drawable.okbook1);
                    isChecked = 1;
                    addCheckBoxValue.set(position,isChecked);
                    price = price + Integer.parseInt(BookPrice.get(position));
                    settings = context.getSharedPreferences("Supporter", 0);
                    SharedPreferences.Editor editor = settings.edit();
                    ArrayList<String> sBookName = new ArrayList<>();
                    ArrayList<String> sBookPrice = new ArrayList<>();
                    ArrayList<String> sBookId = new ArrayList<>();
                    if (!settings.contains("SetBookName")) {

                        sBookName.add(BookName.get(position));
                        sBookPrice.add(BookPrice.get(position));
                        sBookId.add(BookId.get(position));

                    }
                    else{
                        try {
                            JSONArray bookNameA = new JSONArray(settings.getString("SetBookName",""));
                            JSONArray bookPriceA = new JSONArray(settings.getString("SetBookPrice",""));
                            JSONArray bookIdA = new JSONArray(settings.getString("SetBookId",""));

                            for (int i =0;i<bookNameA.length();i++){
                                sBookName.add(bookNameA.getString(i));
                                sBookPrice.add(bookPriceA.getString(i));
                                sBookId.add(bookIdA.getString(i));
                            }
                            sBookName.add(BookName.get(position));
                            sBookPrice.add(BookPrice.get(position));
                            sBookId.add(BookId.get(position));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    JSONArray bookNameJ = new JSONArray(sBookName);
                    JSONArray bookPriceJ = new JSONArray(sBookPrice);
                    JSONArray bookIdJ = new JSONArray(sBookId);

                    editor.putString("SetBookName", bookNameJ.toString());
                    editor.putString("SetBookPrice", bookPriceJ.toString());
                    editor.putString("SetBookId", bookIdJ.toString());
                    editor.commit();
                    editor.apply();


                }
                else{
                    turns.set(position,false);
                    getPic.set(position,R.drawable.nobook1);
                    isChecked = 0;
                    addCheckBoxValue.set(position,isChecked);
                    price = price - Integer.parseInt(BookPrice.get(position));
                    ArrayList<String> sBookName = new ArrayList<>();
                    ArrayList<String> sBookPrice = new ArrayList<>();
                    ArrayList<String> sBookId = new ArrayList<>();
                    settings = context.getSharedPreferences("Supporter",0);
//                    if (!settings.contains("SetBookName")) {
                        try {
                            JSONArray bookNameA = new JSONArray(settings.getString("SetBookName",""));
                            JSONArray bookPriceA = new JSONArray(settings.getString("SetBookPrice",""));
                            JSONArray bookIdA = new JSONArray(settings.getString("SetBookId",""));

                            for (int i =0;i<bookNameA.length();i++){
                                sBookName.add(bookNameA.getString(i));
                                sBookPrice.add(bookPriceA.getString(i));
                                sBookId.add(bookIdA.getString(i));
                            }
                            int index = sBookName.indexOf(BookName.get(position));
                            sBookName.remove(index);
                            sBookPrice.remove(index);
                            sBookId.remove(index);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        JSONArray bookNameJ = new JSONArray(sBookName);
                    JSONArray bookPriceJ = new JSONArray(sBookPrice);
                    JSONArray bookIdJ = new JSONArray(sBookId);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("SetBookName", bookNameJ.toString());
                        editor.putString("SetBookPrice", bookPriceJ.toString());
                        editor.putString("SetBookId", bookIdJ.toString());
                        editor.commit();
                        editor.apply();
//                    }
                }
                Log.e("Name",context.getClass().getSimpleName());
                if (!context.getClass().getSimpleName().equals("CreateBookRecomActivity")) {
                    sumPrice = ((Activity) context).findViewById(R.id.sumPrice);
                    if(turns.get(position))
                        sumPrice.setText("قیمت نهایی: "+price+" تومان ");
                    else
                        sumPrice.setText("قیمت نهایی: "+price+" تومان ");


                }

                notifyDataSetChanged();
            }
        });


        return convertView;
    }



    public class ViewHolder{
        LinearLayout rowBook;
        TextView bookName,bookPrice;
        ImageView getbook;
    }

    public  ArrayList<Integer> getCheckBoxValue(){
        return addCheckBoxValue;
    }

}
