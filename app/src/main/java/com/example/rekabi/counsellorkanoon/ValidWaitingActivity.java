package com.example.rekabi.counsellorkanoon;

import android.app.Dialog;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ValidWaitingActivity extends AppCompatActivity {

    private String enterCode, Code, supId, supPhone, supName;
    private SharedPreferences preferences;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validwaiting);
        getSupportActionBar().hide();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Fabric.with(this, new Crashlytics());
        logUser();

        preferences = this.getSharedPreferences("Supporter", 0);
        Bundle data = getIntent().getExtras();
        if (data != null) {
            supName = data.getString("supName");
            supId = data.getString("supId");
            supPhone = data.getString("supPhone");
        } else {
            if (preferences.contains("supName")) {
                supName = preferences.getString("userName", "");
            }
            supId = preferences.getString("supId", "");
            supPhone = preferences.getString("supPhone", "");
        }



        Button exit = findViewById(R.id.exit);
        Button retry = findViewById(R.id.retry);

        String supActivate = "0";
        SharedPreferences settings = getSharedPreferences("Supporter", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("supActivate", supActivate);
        editor.commit();
        editor.apply();


        CheckActivate();

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Alertdialog_exit();
            }
        });


        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckActivate();
            }
        });




    }



    public void CheckActivate(){

        SpotsDialog alertDialog = new SpotsDialog(ValidWaitingActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        Map<String, String> ActivateParams = new HashMap<String, String>();
        ActivateParams.put("supporterId", supId);
        // //need webservice for check the isActive parameter
        final String CheckActivateUrl = getResources().getString(R.string.url) + "CheckSupporterIsValid";
        new VolleyPost(ValidWaitingActivity.this, CheckActivateUrl, ActivateParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("CheckSupporterIsValid", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    String success = jsonObject.getString("success");

                    if(success.equals("1")){
                        String supActivate = "1";
                        SharedPreferences settings = getSharedPreferences("Supporter", 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("supActivate", supActivate);
                        editor.commit();
                        editor.apply();

                        Intent intent = new Intent(ValidWaitingActivity.this, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("supName", supName);
                        intent.putExtra("supPhone", supPhone);
                        intent.putExtra("supId", supId);
                        startActivity(intent);
                    }
                    else{
                        String supActivate = "0";
                        SharedPreferences settings = getSharedPreferences("Supporter", 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("supActivate", supActivate);
                        editor.commit();
                        editor.apply();
                        Toast.makeText(ValidWaitingActivity.this,"اطلاعات شما هنوز تایید نشده است. لطفا منتظر بمانید!",Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void Alertdialog_exit() {
        final Dialog My_alertdialog = new Dialog(ValidWaitingActivity.this);
        My_alertdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        My_alertdialog.setContentView(R.layout.alert_style_exit);
        Button okRequest = (Button) My_alertdialog.findViewById(R.id.yesExit);
        okRequest.setEnabled(true);
        Button noRequest = (Button) My_alertdialog.findViewById(R.id.noExit);
        noRequest.setEnabled(true);

        okRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences settings = getSharedPreferences("Supporter", MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.clear();
                editor.commit();
                Intent intent = new Intent(ValidWaitingActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
        noRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                My_alertdialog.cancel();
            }
        });
        My_alertdialog.setCancelable(true);
        My_alertdialog.show();

    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        SharedPreferences settings = getSharedPreferences("Supporter", ValidWaitingActivity.this.MODE_PRIVATE);
        Crashlytics.setUserIdentifier(settings.getString("supId", ""));
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
