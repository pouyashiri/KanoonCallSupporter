package com.example.rekabi.counsellorkanoon;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FinalBookRecomActivity extends AppCompatActivity {

    private ArrayList<String> setBookName = new ArrayList<>();
    private ArrayList<String> setBookPrice = new ArrayList<>();
    private ArrayList<String> setBookIds = new ArrayList<>();

    private ArrayList<Integer> setRecomId = new ArrayList<>();
    private ArrayList<Integer> turn = new ArrayList<>();
    private String studentName,reqId,supId;
    private ArrayList<Integer> addCheckBoxValue = new ArrayList<>();
    private TextView tv_studentName,sumPrice;
    private ListView listView;
    private GetBookListAdapter getBookListAdapter;
    private Button sendRecom;
    SpotsDialog alertDialog;
    int Counter ,removeNum=0,price=0;
    private String feedbackDone;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_book_recom);
        getSupportActionBar().hide();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Fabric.with(this, new Crashlytics());
        logUser();

        setBookName = getIntent().getExtras().getStringArrayList("BookNames");
        setBookPrice = getIntent().getExtras().getStringArrayList("BookPrices");
        setBookIds = getIntent().getExtras().getStringArrayList("BookIds");

        setRecomId = getIntent().getExtras().getIntegerArrayList("recomIds");
        studentName = getIntent().getStringExtra("studentName");
        reqId = getIntent().getStringExtra("reqId");
        supId = getIntent().getStringExtra("supId");
        turn.addAll(Collections.nCopies(setBookPrice.size(),0));

        SharedPreferences settings = getSharedPreferences("Supporter", 0);
        SharedPreferences.Editor editor = settings.edit();

        if(supId==null){
            supId = settings.getString("supId","");
            reqId = settings.getString("reqId", "");
            studentName = settings.getString("studentName", "");
        }



        feedbackDone = "1";
        editor.putString("feedbackDone", feedbackDone);
        editor.putString("reqId", reqId);
        editor.putString("studentName", studentName);
        editor.commit();
        editor.apply();

        Log.e("setRecomId::::",setRecomId.toString());


        tv_studentName = findViewById(R.id.tv_studentName2);
        tv_studentName.setText(studentName);
        listView = findViewById(R.id.finalbookList);
        sendRecom = findViewById(R.id.bt_sendRecom);
        sumPrice = findViewById(R.id.sumPrice);


        for (int i = 0; i < setBookName.size(); i++) {
            addCheckBoxValue.add(0);
        }
        getBookListAdapter = new GetBookListAdapter(FinalBookRecomActivity.this, reqId, setBookIds,setBookName, setBookPrice, addCheckBoxValue);
        listView.setAdapter(getBookListAdapter);



        sendRecom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Integer> Value = getBookListAdapter.getCheckBoxValue();
                Log.e("valll",Value.toString());
                createFinalRecom(Value);
                Alertdialog_send();

            }
        });
    }

    public void createFinalRecom(ArrayList<Integer> Values){
        Counter = 0;
        for (int i = 0; i < Values.size(); i++) {
            if (Values.get(i)==0) {
                String recomId = setRecomId.get(i).toString();
                RemoveRecom(recomId);
                Counter = Counter+1;
            }
        }
        removeNum = Counter;

    }


    public void RemoveRecom(String recomId){
        alertDialog = new SpotsDialog(FinalBookRecomActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);


        Map<String, String> Params = new HashMap<String, String>();
        Log.e("remove id:-",recomId+"");
        Params.put("recomId",recomId);
        String Url = getResources().getString(R.string.url)+"RemoveRecommendation";
        new VolleyPost(FinalBookRecomActivity.this, Url, Params, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("RemoveRecom ", Result);
                    JSONObject jsonObject = new JSONObject(Result);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    public void finalizeRecom(final String reqId){
        alertDialog = new SpotsDialog(FinalBookRecomActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);


        Map<String, String> Params = new HashMap<String, String>();
        Params.put("reqId",reqId);
        String Url2 = getResources().getString(R.string.url)+"FinalizeRecommendations";
        new VolleyPost(FinalBookRecomActivity.this, Url2, Params, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("FinalizeRecom ", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    String success = jsonObject.getString("success");
                    if(success.equals("1")){
                        SharedPreferences settings = getSharedPreferences("Supporter", 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.remove("SetBookName");
                        editor.remove("SetBookPrice");
                        editor.remove("SetBookId");

                        feedbackDone = "3";
                        editor.putString("feedbackDone", feedbackDone);
                        editor.putString("reqId", reqId);
                        editor.putString("supId", supId);
                        editor.putString("studentName", studentName);
                        editor.commit();
                        editor.apply();
                        Intent intent = new Intent(FinalBookRecomActivity.this,HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("supId",supId);
                        SharedPreferences setting = getSharedPreferences("Supporter", 0);
                        String Online;
                        if(setting.contains("isOnline")){
                            Online = setting.getString("isOnline", "");
                            Log.e("Online----------","-----"+Online);
                        }else{
                            Online = "1";
                        }
                        intent.putExtra("isOnline",Online);
                        startActivity(intent);
                    }else{
                        Toast.makeText(FinalBookRecomActivity.this,"لطفا مجددا تایید کنید.",Toast.LENGTH_LONG).show();
                    }




                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    public void Alertdialog_send() {
        final Dialog My_alertdialog = new Dialog(FinalBookRecomActivity.this);
        My_alertdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        My_alertdialog.setContentView(R.layout.alert_sendbook);
        TextView removeNumber = My_alertdialog.findViewById(R.id.remove_number);
        Button sendBoos = (Button) My_alertdialog.findViewById(R.id.sendBooks);
        sendBoos.setEnabled(true);
        removeNumber.setText(String.valueOf(removeNum)+" کتاب از لیست توصیه خود را حذف کردید!");

        sendBoos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finalizeRecom(reqId);

                My_alertdialog.cancel();
            }
        });

        My_alertdialog.setCancelable(true);
        My_alertdialog.show();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (alertDialog!=null) {
            alertDialog.dismiss();
            alertDialog=null;
        }
    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        SharedPreferences settings = getSharedPreferences("Supporter", FinalBookRecomActivity.this.MODE_PRIVATE);
        Crashlytics.setUserIdentifier(settings.getString("supId", ""));
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


}
