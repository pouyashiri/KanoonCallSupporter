package com.example.rekabi.counsellorkanoon;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RegisterActivity extends AppCompatActivity {
    private SpotsDialog alertDialog;
    private Button register;
    private EditText etName, etPhoneNumber,etNationalCode,etExamRank,etField,etUniversity;
    private Spinner spSex, spState, spCity;
    private String stateUrl, cityUrl,supId,supName,supPhone;

    private ArrayList<String> stateNameList = new ArrayList<>();
    private ArrayList<String> stateCodeList = new ArrayList<>();
    private ArrayList<String> cityNameList = new ArrayList<>();
    private ArrayList<String> cityCodeList = new ArrayList<>();
    private ArrayList<String> sexList = new ArrayList<>();

    private String supSex, supState, supStateCode, supCity, supCityCode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().hide();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Fabric.with(this, new Crashlytics());
        logUser();


        //Logout:
        SharedPreferences settings = getSharedPreferences("Supporter", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.commit();

        //Register:

        etName = findViewById(R.id.etName);
        etPhoneNumber = findViewById(R.id.etPhoneNumber);
        etNationalCode = findViewById(R.id.etNationalCode);
        etExamRank = findViewById(R.id.etExamRank);
        etField = findViewById(R.id.etField);
        etUniversity = findViewById(R.id.etUniversity);
        spSex = findViewById(R.id.spSex);
        spState = findViewById(R.id.spState);
        spCity = findViewById(R.id.spCity);
        register = findViewById(R.id.registerButton);


//            sexList.add("جنسیت خود را انتخاب نمایید");
            sexList.add("زن");
            sexList.add("مرد");
            ArrayAdapter<String> sexArrayAdapter = new ArrayAdapter<String>(RegisterActivity.this, R.layout.spinner_item, sexList);
            sexArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
            spSex.setAdapter(sexArrayAdapter);


//            stateNameList.add("استان خود را انتخاب نمایید");
//            stateCodeList.add("0");
            cityNameList.add("شهر");
            cityCodeList.add("0");

            ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(RegisterActivity.this, R.layout.spinner_item, cityNameList);
            cityAdapter.setDropDownViewResource(R.layout.spinner_item);
            spCity.setAdapter(cityAdapter);

            stateUrl =getResources().getString(R.string.url) + "GetStates";
            cityUrl = getResources().getString(R.string.url) + "GetCities";


            alertDialog = new SpotsDialog(RegisterActivity.this, R.style.Custom);
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);

            new VolleyPost(RegisterActivity.this, stateUrl, new HashMap<String, String>(), 1000, alertDialog, new VolleyPost.onResponse() {
                @Override
                public void onFinish(String Result) {
                    try {
                        Log.e("state: ", Result);
                        JSONObject jsonObject = new JSONObject(Result);
                        JSONArray jsonArrayResult = jsonObject.getJSONArray("states");
                        for (int i = 0; i < jsonArrayResult.length(); i++) {
                            JSONObject statejsonObject = jsonArrayResult.getJSONObject(i);
                            stateNameList.add(statejsonObject.getString("StateName"));
                            stateCodeList.add(statejsonObject.getString("StateCode"));
                        }
                        ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(RegisterActivity.this, R.layout.spinner_item, stateNameList);
                        stateAdapter.setDropDownViewResource(R.layout.spinner_item);
                        spState.setAdapter(stateAdapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });




            spSex.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                    if (position == 0) {
//                        supSex = "";
//                    } else {
                        if(position==0){
                            supSex = "0";
                        }
                        if(position==1){
                            supSex = "1";
                        }

//                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    supSex = "0";
                }
            });


            spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position < 0) {
                        supState = "";
                        supStateCode = "";
                    } else {
                        cityNameList.clear();
                        cityCodeList.clear();
//                        cityNameList.add("شهر");
//                        cityCodeList.add("0");
                        supState = stateNameList.get(position);
                        supStateCode = stateCodeList.get(position);

                        Map<String, String> stateMap = new HashMap<String, String>();
                        stateMap.put("stateCode", supStateCode);
                        stateMap.put("year", "98");

                        new VolleyPost(RegisterActivity.this, cityUrl, stateMap, 1000, alertDialog, new VolleyPost.onResponse() {
                            @Override
                            public void onFinish(String Result) {
                                try {
                                    Log.e("res", Result);
                                    JSONObject jsonObject = new JSONObject(Result);
                                    JSONArray jsonArrayResult = jsonObject.getJSONArray("cities");
                                    for (int i = 0; i < jsonArrayResult.length(); i++) {
                                        JSONObject cityjsonObject = jsonArrayResult.getJSONObject(i);
                                        cityNameList.add(cityjsonObject.getString("CityName"));
                                        cityCodeList.add(cityjsonObject.getString("CityCode"));
                                    }
                                    ArrayAdapter<String> cityAdapter = new ArrayAdapter<String>(RegisterActivity.this, R.layout.spinner_item, cityNameList);
                                    cityAdapter.setDropDownViewResource(R.layout.spinner_item);
                                    spCity.setAdapter(cityAdapter);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    supState = stateNameList.get(0);
                    supStateCode = stateCodeList.get(0);
                }
            });


            spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position < 0) {
                        supCity = "";
                        supCityCode = "";
                    } else {
                        supCity = cityNameList.get(position);
                        supCityCode = cityCodeList.get(position);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    supCity = cityNameList.get(0);
                    supCityCode = cityCodeList.get(0);
                }
            });


            register.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String supMobile = etPhoneNumber.getText().toString();
                    String supFullName = etName.getText().toString();
                    String supExamRank = etExamRank.getText().toString();
                    String supField = etField.getText().toString();
                    String supUniversity = etUniversity.getText().toString();
                    String supNationalCode = etNationalCode.getText().toString();

                    if (supMobile.length() != 11 || !supMobile.startsWith("09")) {
                        Toast.makeText(RegisterActivity.this, "شماره همراه شما معتبر نمی باشد!", Toast.LENGTH_SHORT).show();
                        supMobile = "";
                    } else if ((supMobile.length() == 11) && supMobile.startsWith("09")) {
                        supMobile = etPhoneNumber.getText().toString();
                    }

                    if(supNationalCode.length()!=10){
                        Toast.makeText(RegisterActivity.this, "کد ملی شما معتبر نمی باشد!", Toast.LENGTH_SHORT).show();
                        supNationalCode = "";
                    } else if ((supMobile.length() == 11)) {
                        supMobile = etPhoneNumber.getText().toString();
                    }



                    if (supFullName == "" || supMobile == "" || supSex == "" || supState == "" || supStateCode == "" || supCity == ""
                            || supCityCode == "" || supField == "" || supUniversity == "" || supNationalCode=="" || supExamRank=="") {
                        Toast.makeText(RegisterActivity.this, "لطفا تمامی فیلدها را وارد نمایید!", Toast.LENGTH_LONG).show();
                    } else {

                        Map<String, String> params = new HashMap<String, String>();
                        params.put("mobile", supMobile);
                        params.put("sex", supSex);
                        Log.e("sex",supSex);
                        params.put("fullName", supFullName);
                        params.put("statename", supState);
                        params.put("cityname", supCity);
                        params.put("statecode", supStateCode);
                        params.put("citycode", supCityCode);
                        params.put("nationalcode", supNationalCode);
                        params.put("university", supUniversity);
                        params.put("field", supField);
                        params.put("examrank", supExamRank);


                        String registerUrl = getResources().getString(R.string.url) + "RegisterSupporter";
                        new VolleyPost(RegisterActivity.this, registerUrl, params, 1000, alertDialog, new VolleyPost.onResponse() {
                            @Override
                            public void onFinish(String Result) {
                                try {
                                    Log.e("register", Result);
                                    JSONObject jsonObject = new JSONObject(Result);
                                    String success = jsonObject.getString("success");

                                    if(success.equals("1")){
                                        supId = jsonObject.getString("supporterId");
                                        supName = jsonObject.getString("supName");
                                        supPhone = jsonObject.getString("supPhone");

                                    }else{
                                        JSONObject sup = jsonObject.getJSONObject("supporter");
                                        supId = sup.getString("Id");
                                        supName = sup.getString("FullName");
                                        supPhone = sup.getString("Mobile");
                                    }


                                    SharedPreferences settings = getSharedPreferences("Supporter", 0);
                                    SharedPreferences.Editor editor = settings.edit();
                                    editor.putString("supName", supName);
                                    editor.putString("supPhone", supPhone);
                                    editor.putString("supId", supId);
                                    editor.commit();
                                    editor.apply();

                                    Intent intent = new Intent(RegisterActivity.this, SmsActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.putExtra("supName", supName);
                                    intent.putExtra("supPhone", supPhone);
                                    intent.putExtra("supId", supId);
                                    startActivity(intent);


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            });
//        }
        }








    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (alertDialog!=null) {
            alertDialog.dismiss();
            alertDialog=null;
        }
    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        SharedPreferences settings = getSharedPreferences("Supporter", RegisterActivity.this.MODE_PRIVATE);
        Crashlytics.setUserIdentifier(settings.getString("supId", ""));
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    }