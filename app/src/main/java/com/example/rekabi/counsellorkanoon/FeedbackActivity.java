package com.example.rekabi.counsellorkanoon;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FeedbackActivity extends AppCompatActivity {

    private int turnReport=1;
    private SpotsDialog alertDialog;
    private TextView tv_studentName, txt1, txt2, txt3, txt4, txt5, txt6, txt7, txt8, txt9;
    private ImageView feedback_exam, feedback_book, feedback_nothing,
            feedback_sup,feedback_custom,feedback_teach,feedback_school;
    private String reqId, supId, studentName , duration;
    private String feedbackDone = "0";
    private int BookChecked = 0, ExamChecked = 0, SupChecked = 0, CustomChecked = 0, TeachChecked = 0, SchoolChecked = 0;
    int turnBook = 1, turnExam = 1, turnSup = 1, turnCustom = 1, turnTeach = 1, turnSchool = 1;
    int needMoreTurn = 1, dontLikeTurn = 1;
    private int needMoreSupporter = 0, dontLikeSupporter = 0;
    ImageView needMorSup, dontLikeSup;

    private ArrayList<String> FeedbackItems = new ArrayList<>();
    private ArrayList<Integer> ItemId = new ArrayList<>();
    private ArrayList<Integer> ItemValue = new ArrayList<>();

    private LinearLayout less40sec,more40sec;
    private Button less40button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        getSupportActionBar().hide();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Fabric.with(this, new Crashlytics());
        logUser();

        less40sec = findViewById(R.id.less40sec);
        less40button = findViewById(R.id.less40button);
        more40sec = findViewById(R.id.more40sec);
        less40sec.setVisibility(View.INVISIBLE);
        more40sec.setVisibility(View.INVISIBLE);

        tv_studentName = findViewById(R.id.StudentName);
        feedback_exam = findViewById(R.id.feedback_exam);
        feedback_exam.setImageDrawable(getResources().getDrawable(R.drawable.examchecked));
        feedback_book = findViewById(R.id.feedback_book);
        feedback_book.setImageDrawable(getResources().getDrawable(R.drawable.bookchecked));
        feedback_nothing = findViewById(R.id.feedback_nothing);
        feedback_nothing.setImageDrawable(getResources().getDrawable(R.drawable.nothingchecked));


        feedback_sup = findViewById(R.id.feedback_sup);
        feedback_sup.setImageDrawable(getResources().getDrawable(R.drawable.examchecked));

        feedback_custom = findViewById(R.id.feedback_custom);
        feedback_custom.setImageDrawable(getResources().getDrawable(R.drawable.examchecked));

        feedback_teach = findViewById(R.id.feedback_teach);
        feedback_teach.setImageDrawable(getResources().getDrawable(R.drawable.examchecked));

        feedback_school = findViewById(R.id.feedback_school);
        feedback_school.setImageDrawable(getResources().getDrawable(R.drawable.examchecked));

        Button RegisterFeedback1 = findViewById(R.id.RegisterFeedback1);
        txt1 = findViewById(R.id.txt1);
        txt1.setTextColor(getResources().getColor(R.color.gray));
        txt2 = findViewById(R.id.txt2);
        txt2.setTextColor(getResources().getColor(R.color.gray));
        txt3 = findViewById(R.id.txt3);
        txt3.setTextColor(getResources().getColor(R.color.gray));

        txt6 = findViewById(R.id.txt6);
        txt6.setTextColor(getResources().getColor(R.color.gray));
        txt7 = findViewById(R.id.txt7);
        txt7.setTextColor(getResources().getColor(R.color.gray));
        txt8 = findViewById(R.id.txt8);
        txt8.setTextColor(getResources().getColor(R.color.gray));
        txt9 = findViewById(R.id.txt9);
        txt9.setTextColor(getResources().getColor(R.color.gray));


        SharedPreferences settings = getSharedPreferences("Supporter", 0);
        if(settings.contains("reportCall")){
            String report = settings.getString("reportCall","");
            int rep = Integer.parseInt(report);
            if(rep==1){turnReport=1;}
            if(rep==2){turnReport=2;}

        }else{
            SharedPreferences.Editor editor = settings.edit();
            turnReport=1;
            editor.putString("reportCall",turnReport+"");
            editor.commit();
            editor.apply();
        }



        Bundle data = getIntent().getExtras();
        if (data != null) {
            studentName = data.getString("studentName");
            reqId = data.getString("reqId");
            supId = data.getString("supId");
            duration = data.getString("duration");
        }
        if(supId==null){
            supId = settings.getString("supId","");
            reqId = settings.getString("reqId", "");
            studentName = settings.getString("studentName", "");
            duration = settings.getString("duration","");
        }
        /////////////
//        studentName = "مریم رکابی";
//        reqId = "74";
//        Log.e(":reqId is:", reqId);
        /////////////
        tv_studentName.setText("دانش آموز: " + studentName);


        GetFeedBackItems();


        SharedPreferences settings1 = getSharedPreferences("Supporter", 0);
        SharedPreferences.Editor editor = settings1.edit();
        feedbackDone = "0";
        editor.putString("feedbackDone", feedbackDone);
        editor.putString("reqId", reqId);
        editor.putString("supId", supId);
        editor.putString("studentName", studentName);
        editor.putString("duration", duration);
        editor.commit();
        editor.apply();

        int Dur = Integer.parseInt(duration);
        Log.e("dur",Dur+"-");
        if(Dur < 40){
            less40sec.setVisibility(View.VISIBLE);
            more40sec.setVisibility(View.INVISIBLE);
            SharedPreferences settings0 = getSharedPreferences("Supporter", 0);
            if(settings0.contains("reportCall")){
                String report = settings0.getString("reportCall","");
                int rep = Integer.parseInt(report);
                if(rep==1){turnReport=1;}
                if(rep==2){turnReport=2;}
                turnReport=1;
//                if(turnReport==1){
                    ReportCall(reqId,duration);
//                }
            }
        }
        if(Dur >= 40){
            less40sec.setVisibility(View.INVISIBLE);
            more40sec.setVisibility(View.VISIBLE);
            SharedPreferences setting = getSharedPreferences("Supporter", 0);
            if(setting.contains("reportCall")){
                String report = setting.getString("reportCall","");
                int rep = Integer.parseInt(report);
                if(rep==1){turnReport=1;}
                if(rep==2){turnReport=2;}
//                if(turnReport==1){
                    ReportCall(reqId,duration);
//                }
            }
        }

        less40button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(FeedbackActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("studentName", studentName);
                intent.putExtra("supId", supId);
                SharedPreferences setting = getSharedPreferences("Supporter", 0);
                String Online;
                if(setting.contains("isOnline")){
                    Online = setting.getString("isOnline", "");
                    Log.e("Online----------","-----"+Online);
                }else{
                    Online = "1";
                }
                intent.putExtra("isOnline",Online);
                startActivity(intent);
            }
        });

        feedback_book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (turnBook == 1) {
                    txt2.setTextColor(getResources().getColor(R.color.Red));
                    feedback_book.setImageDrawable(getResources().getDrawable(R.drawable.book));
                    BookChecked = 1;
                    turnBook = 2;
                } else if (turnBook == 2) {
                    txt2.setTextColor(getResources().getColor(R.color.gray));
                    feedback_book.setImageDrawable(getResources().getDrawable(R.drawable.bookchecked));
                    BookChecked = 0;
                    turnBook = 1;
                }
            }
        });

        feedback_exam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (turnExam == 1) {
                    txt1.setTextColor(getResources().getColor(R.color.Red));
                    feedback_exam.setImageDrawable(getResources().getDrawable(R.drawable.exam));
                    ExamChecked = 1;
                    turnExam = 2;
                } else if (turnExam == 2) {
                    txt1.setTextColor(getResources().getColor(R.color.gray));
                    feedback_exam.setImageDrawable(getResources().getDrawable(R.drawable.examchecked));
                    ExamChecked = 0;
                    turnExam = 1;
                }
            }
        });

        feedback_sup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (turnSup == 1) {
                    txt6.setTextColor(getResources().getColor(R.color.Red));
                    feedback_sup.setImageDrawable(getResources().getDrawable(R.drawable.exam));
                    SupChecked = 1;
                    turnSup = 2;
                } else if (turnSup == 2) {
                    txt6.setTextColor(getResources().getColor(R.color.gray));
                    feedback_sup.setImageDrawable(getResources().getDrawable(R.drawable.examchecked));
                    SupChecked = 0;
                    turnSup = 1;
                }
            }
        });

        feedback_custom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (turnCustom == 1) {
                    txt7.setTextColor(getResources().getColor(R.color.Red));
                    feedback_custom.setImageDrawable(getResources().getDrawable(R.drawable.exam));
                    CustomChecked = 1;
                    turnCustom = 2;
                } else if (turnCustom == 2) {
                    txt7.setTextColor(getResources().getColor(R.color.gray));
                    feedback_custom.setImageDrawable(getResources().getDrawable(R.drawable.examchecked));
                    CustomChecked = 0;
                    turnCustom = 1;
                }
            }
        });

        feedback_teach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (turnTeach == 1) {
                    txt8.setTextColor(getResources().getColor(R.color.Red));
                    feedback_teach.setImageDrawable(getResources().getDrawable(R.drawable.exam));
                    TeachChecked = 1;
                    turnTeach = 2;
                } else if (turnTeach == 2) {
                    txt8.setTextColor(getResources().getColor(R.color.gray));
                    feedback_teach.setImageDrawable(getResources().getDrawable(R.drawable.examchecked));
                    TeachChecked = 0;
                    turnTeach= 1;
                }
            }
        });

        feedback_school.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (turnSchool == 1) {
                    txt9.setTextColor(getResources().getColor(R.color.Red));
                    feedback_school.setImageDrawable(getResources().getDrawable(R.drawable.exam));
                    SchoolChecked = 1;
                    turnSchool = 2;
                } else if (turnSchool == 2) {
                    txt9.setTextColor(getResources().getColor(R.color.gray));
                    feedback_school.setImageDrawable(getResources().getDrawable(R.drawable.examchecked));
                    SchoolChecked = 0;
                    turnSchool = 1;
                }
            }
        });

        feedback_nothing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt3.setTextColor(getResources().getColor(R.color.Red));
                feedback_nothing.setImageDrawable(getResources().getDrawable(R.drawable.nothing1));
                Alertdialog_Feedback();
            }
        });

        RegisterFeedback1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ///////////////
                needMoreSupporter = 0;
                dontLikeSupporter = 0;
                ItemValue.clear();
                ItemValue.add(ExamChecked);
                ItemValue.add(BookChecked);
                ItemValue.add(needMoreSupporter);
                ItemValue.add(dontLikeSupporter);
                ItemValue.add(SupChecked);
                ItemValue.add(CustomChecked);
                ItemValue.add(TeachChecked);
                ItemValue.add(SchoolChecked);
                JSONArray jsonArray = new JSONArray(ItemValue);
                String value0 = jsonArray.toString();
                String Values = value0.substring(1, value0.length() - 1);

                JSONArray arrayid = new JSONArray(ItemId);
                String itemId0 = arrayid.toString();
                String feedbackIds = itemId0.substring(1, itemId0.length() - 1);

                RegisterSupporterFeedback(ExamChecked,BookChecked, reqId, feedbackIds, Values);

            }
        });


    }


    public void Alertdialog_Feedback() {
        final Dialog My_alertdialog = new Dialog(FeedbackActivity.this);
        My_alertdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        My_alertdialog.setContentView(R.layout.alert_feedback);
        needMorSup = My_alertdialog.findViewById(R.id.needMoreSup);
        dontLikeSup = My_alertdialog.findViewById(R.id.dontLikeSup);
        Button RegisterFeedback2 = My_alertdialog.findViewById(R.id.RegisterFeedback2);
        txt4 = My_alertdialog.findViewById(R.id.txt4);
        txt4.setTextColor(getResources().getColor(R.color.gray));
        txt5 = My_alertdialog.findViewById(R.id.txt5);
        txt5.setTextColor(getResources().getColor(R.color.gray));

        txt4.setText(FeedbackItems.get(2));
        txt5.setText(FeedbackItems.get(3));
        needMorSup.setImageDrawable(getResources().getDrawable(R.drawable.needmore1));
        dontLikeSup.setImageDrawable(getResources().getDrawable(R.drawable.dontlike1));


        needMorSup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (needMoreTurn == 1) {
                    txt4.setTextColor(getResources().getColor(R.color.Red));
                    needMorSup.setImageDrawable(getResources().getDrawable(R.drawable.needmore));
                    needMoreSupporter = 1;
                    needMoreTurn = 2;
                } else if (needMoreTurn == 2) {
                    txt4.setTextColor(getResources().getColor(R.color.gray));
                    needMorSup.setImageDrawable(getResources().getDrawable(R.drawable.needmore1));
                    needMoreSupporter = 0;
                    needMoreTurn = 1;
                }
            }
        });
        dontLikeSup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dontLikeTurn == 1) {
                    txt5.setTextColor(getResources().getColor(R.color.Red));
                    dontLikeSup.setImageDrawable(getResources().getDrawable(R.drawable.dontlike));
                    dontLikeSupporter = 1;
                    dontLikeTurn = 2;
                } else if (dontLikeTurn == 2) {
                    txt5.setTextColor(getResources().getColor(R.color.gray));
                    dontLikeSup.setImageDrawable(getResources().getDrawable(R.drawable.dontlike1));
                    dontLikeSupporter = 0;
                    dontLikeTurn = 1;
                }
            }
        });
        RegisterFeedback2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /////////////
                ExamChecked = 0;
                BookChecked = 0;
                SupChecked = 0;
                CustomChecked = 0;
                TeachChecked = 0;
                SchoolChecked = 0;
                needMoreSupporter = needMoreSupporter;
                dontLikeSupporter = dontLikeSupporter;
                ItemValue.clear();
                ItemValue.add(ExamChecked);
                ItemValue.add(BookChecked);
                ItemValue.add(needMoreSupporter);
                ItemValue.add(dontLikeSupporter);
                ItemValue.add(SupChecked);
                ItemValue.add(CustomChecked);
                ItemValue.add(TeachChecked);
                ItemValue.add(SchoolChecked);
                JSONArray jsonArray = new JSONArray(ItemValue);
                String value0 = jsonArray.toString();
                String Values = value0.substring(1, value0.length() - 1);

                JSONArray arrayid = new JSONArray(ItemId);
                String itemId0 = arrayid.toString();
                String feedbackIds = itemId0.substring(1, itemId0.length() - 1);


                RegisterSupporterFeedback(ExamChecked,BookChecked, reqId, feedbackIds, Values);
                My_alertdialog.cancel();


            }
        });
        My_alertdialog.setCancelable(true);
        My_alertdialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                txt3.setTextColor(getResources().getColor(R.color.gray));
                feedback_nothing.setImageDrawable(getResources().getDrawable(R.drawable.nothingchecked));

                txt1.setTextColor(getResources().getColor(R.color.gray));
                feedback_exam.setImageDrawable(getResources().getDrawable(R.drawable.examchecked));
                ExamChecked = 0;
                turnExam = 1;
                txt2.setTextColor(getResources().getColor(R.color.gray));
                feedback_book.setImageDrawable(getResources().getDrawable(R.drawable.bookchecked));
                BookChecked = 0;
                turnBook = 1;
                txt6.setTextColor(getResources().getColor(R.color.gray));
                feedback_sup.setImageDrawable(getResources().getDrawable(R.drawable.examchecked));
                SupChecked = 0;
                turnSup = 1;
                txt7.setTextColor(getResources().getColor(R.color.gray));
                feedback_custom.setImageDrawable(getResources().getDrawable(R.drawable.examchecked));
                CustomChecked = 0;
                turnCustom = 1;
                txt8.setTextColor(getResources().getColor(R.color.gray));
                feedback_teach.setImageDrawable(getResources().getDrawable(R.drawable.examchecked));
                TeachChecked = 0;
                turnTeach= 1;
                txt9.setTextColor(getResources().getColor(R.color.gray));
                feedback_school.setImageDrawable(getResources().getDrawable(R.drawable.examchecked));
                SchoolChecked = 0;
                turnSchool = 1;


            }
        });
        My_alertdialog.show();

    }


    private void ReportCall(String reqId0, final String duration) {
        SpotsDialog alertDialog = new SpotsDialog(FeedbackActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        Map<String, String> ReportCallParams = new HashMap<String, String>();
        ReportCallParams.put("reqId", reqId0);
        ReportCallParams.put("duration", duration);
        final String ReportCallUrl = getResources().getString(R.string.url) + "ReportCall";
        new VolleyPost(FeedbackActivity.this, ReportCallUrl, ReportCallParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("ReportCall: ", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    String success = jsonObject.getString("success");
                    SharedPreferences settings = getSharedPreferences("Supporter",0);
                    SharedPreferences.Editor editor = settings.edit();
                    if (success.equals("1")) {


                        int Dur = Integer.parseInt(duration);
                        if(Dur < 40){
                            turnReport = 1;
                            feedbackDone = "3";
                            more40sec.setVisibility(View.INVISIBLE);
                            less40sec.setVisibility(View.VISIBLE);
                            editor.putString("reportCall",turnReport+"");
                            editor.putString("feedbackDone", feedbackDone);
                            editor.putString("supId", supId);
                            editor.commit();
                            editor.apply();

                        }else if (Dur >= 40) {
                            turnReport = 2;
//                            ////register feedback
                            more40sec.setVisibility(View.VISIBLE);
                            less40sec.setVisibility(View.INVISIBLE);
                            editor.putString("reportCall",turnReport+"");
                            editor.putString("supId", supId);
                            editor.commit();
                            editor.apply();
                        }



                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }



    public void GetFeedBackItems() {

        alertDialog = new SpotsDialog(FeedbackActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        Map<String, String> itemParams = new HashMap<String, String>();
        String isUser = "0";
        itemParams.put("isUser", isUser);

        final String feedbackItemUrl = getResources().getString(R.string.url) + "GetFeedBackItems";
        new VolleyPost(FeedbackActivity.this, feedbackItemUrl, itemParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("GetFeedBackItems", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    JSONArray items = jsonObject.getJSONArray("items");
                    for (int i = 0; i < items.length(); i++) {
                        JSONObject jsonObject1 = items.getJSONObject(i);
                        FeedbackItems.add(jsonObject1.getString("Title"));
                        ItemId.add(jsonObject1.getInt("Id"));
                    }

                    txt1.setText(FeedbackItems.get(0));
                    txt2.setText(FeedbackItems.get(1));
                    txt3.setText("هیچکدام");
                    txt6.setText(FeedbackItems.get(4));
                    txt7.setText(FeedbackItems.get(5));
                    txt8.setText(FeedbackItems.get(6));
                    txt9.setText(FeedbackItems.get(7));


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void RegisterSupporterFeedback(final int examChecked,final int bookChecked, final String reqId, String feedbackIds, String values) {
        alertDialog = new SpotsDialog(FeedbackActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        Map<String, String> FeedbackParams = new HashMap<String, String>();
        FeedbackParams.put("reqId", reqId);
        FeedbackParams.put("feedbackIds", feedbackIds);
        FeedbackParams.put("values", values);

        final String FeedBackUrl = getResources().getString(R.string.url) + "RegisterSupporterFeedback";
        new VolleyPost(FeedbackActivity.this, FeedBackUrl, FeedbackParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("SupporterFeedback : ", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    String success = jsonObject.getString("success");
                    if (success.equals("1")) {
                        if(examChecked==1){
                            ActiveKanoonRegister(reqId);
                        }
                        if (bookChecked == 1) {
                            turnReport=2;
                            feedbackDone = "1";
                            SharedPreferences settings = getSharedPreferences("Supporter", 0);
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putString("feedbackDone", feedbackDone);
                            editor.putString("reportCall",turnReport+"");
                            editor.putString("supId", supId);
                            editor.commit();
                            editor.apply();

                            Intent intent = new Intent(FeedbackActivity.this, CreateBookRecomActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("reqId", reqId);
                            intent.putExtra("supId", supId);
                            intent.putExtra("studentName", studentName);
                            startActivity(intent);
                        } else if (bookChecked == 0) {
                            turnReport=2;
                            if(examChecked==0) {
                                Toast.makeText(FeedbackActivity.this, "نظرسنجی شما به ثبت رسید.", Toast.LENGTH_LONG).show();
                            }
                            feedbackDone = "3";
                            SharedPreferences settings = getSharedPreferences("Supporter", 0);
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putString("feedbackDone", feedbackDone);
                            editor.putString("reportCall",turnReport+"");
                            editor.putString("supId", supId);
                            editor.commit();
                            editor.apply();

                            Intent intent = new Intent(FeedbackActivity.this, HomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("studentName", studentName);
                            intent.putExtra("supId", supId);
                            String Online;
                            if(settings.contains("isOnline")){
                                Online = settings.getString("isOnline", "");
                                Log.e("Online----------","-----"+Online);
                            }else{
                                Online = "1";
                            }
                            intent.putExtra("isOnline",Online);
                            startActivity(intent);
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public void ActiveKanoonRegister(String reqId){


        Map<String, String> Params = new HashMap<String, String>();
        Params.put("reqId", reqId);

        final String Url1 = getResources().getString(R.string.url) + "ActiveKanoonRegister";
        new VolleyPost(FeedbackActivity.this, Url1, Params, 1000, null, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("ActiveKanoonRegister", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    String success = jsonObject.getString("success");
                    if(success.equals("1")){
                        Toast.makeText(FeedbackActivity.this,"پنل ثبت نام برای دانش آموز فعال گردیده و نظرسنجی شما به ثبت رسید.",Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }
    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        SharedPreferences settings = getSharedPreferences("Supporter", FeedbackActivity.this.MODE_PRIVATE);
        Crashlytics.setUserIdentifier(settings.getString("supId", ""));
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
