package com.example.rekabi.counsellorkanoon;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends AppCompatActivity {

    private EditText etsupMobile,etsupPass;
    private Button login,register;
    private String phoneNumber, nationalcode, supId;
    private SpotsDialog alertDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Fabric.with(this, new Crashlytics());
        logUser();

        if (getIntent().getBooleanExtra("EXIT", false)) {
            finish();
            System.exit(0);
        }

        etsupMobile = findViewById(R.id.et_supMobile);
        etsupPass = findViewById(R.id.et_supPass);
        login = findViewById(R.id.LoginButton);
        register = findViewById(R.id.goToRegister);


        SharedPreferences settings = getSharedPreferences("Supporter", 0);
        if ((settings.contains("supActivate"))) {
            String supActivate = settings.getString("supActivate", "");
            if (supActivate.equals("1")){
                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
            else if(supActivate.equals("0")){
                Intent intent = new Intent(LoginActivity.this, ValidWaitingActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        }

        else {

            register.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            });

            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    phoneNumber = etsupMobile.getText().toString();
                    nationalcode = etsupPass.getText().toString();

                    if (phoneNumber.length() != 11 || !phoneNumber.startsWith("09")) {
                        Toast.makeText(LoginActivity.this, "شماره همراه شما معتبر نمی باشد!", Toast.LENGTH_SHORT).show();
                        phoneNumber = "";
                    } else if ((phoneNumber.length() == 11) && phoneNumber.startsWith("09")) {
                        phoneNumber = etsupMobile.getText().toString();
                    }


                    alertDialog = new SpotsDialog(LoginActivity.this, R.style.Custom);
                    alertDialog.setCancelable(false);
                    alertDialog.setCanceledOnTouchOutside(false);

                    Map<String, String> loginParams = new HashMap<String, String>();
                    loginParams.put("nationalcode", nationalcode);
                    loginParams.put("mobile", phoneNumber);
                    final String loginUrl = getResources().getString(R.string.url) + "LoginSupporter";
                    new VolleyPost(LoginActivity.this, loginUrl, loginParams, 1000, alertDialog, new VolleyPost.onResponse() {
                        @Override
                        public void onFinish(String Result) {
                            try {
                                Log.e("login", Result);
                                JSONObject jsonObject = new JSONObject(Result);
                                ///////////////////////////////
                                String login_success = jsonObject.getString("success");


                                Log.e("login_success + :::", login_success+"--");


                                if (login_success.equals("0")) {
                                    Toast.makeText(LoginActivity.this, "شماره همراه شما به ثبت نرسیده، لطفا ثبت نام کنید!", Toast.LENGTH_LONG).show();
                                } else if (login_success.equals("1")) {
                                    JSONObject sup = jsonObject.getJSONObject("supporter");
                                    String supId = sup.getString("Id");
                                    String SupporterName = sup.getString("FullName");

                                    SharedPreferences settings = getSharedPreferences("Supporter", MODE_PRIVATE);
                                    SharedPreferences.Editor editor = settings.edit();
                                    editor.putString("supName", SupporterName);
                                    editor.putString("supPhone", phoneNumber);
                                    editor.putString("supId", supId);
                                    editor.commit();
                                    editor.apply();

                                    Intent intent = new Intent(LoginActivity.this, ValidWaitingActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.putExtra("supName", SupporterName);
                                    intent.putExtra("supPhone", phoneNumber);
                                    intent.putExtra("supId", supId);
                                    startActivity(intent);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });


                }
            });

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (alertDialog!=null) {
            alertDialog.dismiss();
            alertDialog=null;
        }
    }


    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        SharedPreferences settings = getSharedPreferences("Supporter", LoginActivity.this.MODE_PRIVATE);
        Crashlytics.setUserIdentifier(settings.getString("supId", ""));
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


}
