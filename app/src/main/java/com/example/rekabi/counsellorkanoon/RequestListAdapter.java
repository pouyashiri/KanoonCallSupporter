package com.example.rekabi.counsellorkanoon;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.CallLog;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.acl.LastOwnerException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;

public class RequestListAdapter extends BaseAdapter {
    int turn0 = 0;
    public static final Integer CALL = 0x1;
    private int prev_state = TelephonyManager.CALL_STATE_OFFHOOK;
    static long start_time;
    static long end_time;
    static long duration;
    String lastCalledNumber;
    String lastCalledDuration;
    String lastCalledType;
    Context context;
    int pos, j = 1;
    String supId, reqId1, incoming_number, studentMobile,stdName,stdGrade;
    ArrayList<String> studentName;
    ArrayList<String> studentPhone;
    ArrayList<String> studentGrade;
    ArrayList<String> reqId;
    ArrayList<String> slotTime;
    ArrayList<String> lastCalledNumberList = new ArrayList<>();
    ArrayList<String> lastCalledTypeList = new ArrayList<>();
    ArrayList<String> lastCalledDurationList = new ArrayList<>();
    LayoutInflater inflater;
    private int turnreport = 1;


    public RequestListAdapter(Context context, ArrayList<String> reqId, ArrayList<String> studentName, ArrayList<String> studentPhone, ArrayList<String> studentGrade, ArrayList<String> slotTime, String supId) {
        this.context = context;
        this.reqId = reqId;
        this.studentName = studentName;
        this.studentPhone = studentPhone;
        this.studentGrade = studentGrade;
        this.slotTime = slotTime;
        this.supId = supId;
        inflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return studentName.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.row_request, null);
            viewHolder.call = convertView.findViewById(R.id.call);
            viewHolder.slotTime = convertView.findViewById(R.id.SlotTime);
            viewHolder.studentName = convertView.findViewById(R.id.Student_Name);
            viewHolder.studentGrade = convertView.findViewById(R.id.Student_Grade);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.studentName.setText(studentName.get(position));
        viewHolder.studentGrade.setText(studentGrade.get(position));
        viewHolder.slotTime.setText(slotTime.get(position));
        final Typeface tvFont = Typeface.createFromAsset(context.getAssets(), "fonts/B Traffic Bold_p30download.com.ttf");
        viewHolder.studentName.setTypeface(tvFont);
        viewHolder.studentGrade.setTypeface(tvFont);
        viewHolder.slotTime.setTypeface(tvFont);


        viewHolder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Call to student phone
                reqId1 = reqId.get(position);
                studentMobile = studentPhone.get(position);
                stdName = studentName.get(position);
                stdGrade = studentGrade.get(position);

                Log.e("studentMobile",studentMobile);
                Log.e("studentName",stdName);
                Log.e("stdGrade",stdGrade);

                SharedPreferences settings = context.getSharedPreferences("Supporter", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("stdMobile", studentMobile);
                editor.putString("stdName", stdName);
                editor.putString("stdGrade", stdGrade);
                editor.commit();
                editor.apply();

                pos = position;
                CheckRequestValidAndCall(reqId1, position);

            }
        });

        context.registerReceiver(this.broadcastReceiver, new IntentFilter(TelephonyManager.ACTION_PHONE_STATE_CHANGED));

        return convertView;
    }


    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {


//            String action = intent.getAction();
//            if (action.equalsIgnoreCase("android.intent.action.PHONE_STATE")) {
//                if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(
//                        TelephonyManager.EXTRA_STATE_RINGING)) {
//                    start_time = System.currentTimeMillis();
//                }
//                if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(
//                        TelephonyManager.EXTRA_STATE_IDLE)) {
//                    end_time = System.currentTimeMillis();
//                    //Total time talked =
////                    long total_time = end_time - start_time;
//                    //Store total_time somewhere or pass it to an Activity using intent
//                }
//            }


//            String state0 = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
//
//            Log.e("sate = ",state0+"--");
//            if (state0.equals("OFFHOOK")) {
//                //Outgoing call
//                incoming_number = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
//            }
//            Log.i("tag", "Outgoing number : " + incoming_number);


            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            RequestListAdapter.CustomPhoneStateListener customPhoneListener = new RequestListAdapter.CustomPhoneStateListener();
            telephonyManager.listen(customPhoneListener, PhoneStateListener.LISTEN_CALL_STATE); //Register our listener with TelephonyManager

        }
    };


    public class CustomPhoneStateListener extends PhoneStateListener {


        private static final String TAG = "CustomPhoneStateListener";

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {

////            if (incomingNumber != null && incomingNumber.length() > 0){
////                incoming_number = incomingNumber;
////            }
//
////
////            if (incoming_number != null && incoming_number.startsWith("+")) {
////                if (studentMobile.startsWith("0")) {
////                    studentMobile = studentMobile.replaceFirst("0", "+98");
////                }
////            }

            if (state == TelephonyManager.CALL_STATE_RINGING) {
                Log.e("TAG", "CALL_STATE_RINGING");
                prev_state = state;
            } else if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                Log.e("TAG", "CALL_STATE_OFFHOOK");
                prev_state = state;
            } else if (state == TelephonyManager.CALL_STATE_IDLE) {

                if ((prev_state == TelephonyManager.CALL_STATE_OFFHOOK)) {
                    prev_state = TelephonyManager.CALL_STATE_IDLE;
                    /////////////////// CALL LOG read /////////////////
                    //// permision

                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {

//                            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
//                                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_CALL_LOG}, 1);
//                            } else {


                                @SuppressLint("MissingPermission") Cursor mcursor = context.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, null);
                                int number = mcursor.getColumnIndex(CallLog.Calls.NUMBER);
                                int duration = mcursor.getColumnIndex(CallLog.Calls.DURATION);
                                int stateCall = mcursor.getColumnIndex(CallLog.Calls.TYPE);
//                            Log.e("number", number + "--");
                                if (mcursor != null) {
//                            mcursor.moveToFirst();

                                    while (mcursor.moveToNext()) {
                                        lastCalledDurationList.add(mcursor.getString(duration));
                                        lastCalledNumberList.add(mcursor.getString(number));
                                        lastCalledTypeList.add(mcursor.getString(stateCall));
//                                    lastCalledNumber = mcursor.getString(number);
//                                    lastCalledType = mcursor.getString(stateCall);
//                                    lastCalledDuration = mcursor.getString(duration);
//                                    String dir = null;
//                                    int dircode = Integer.parseInt(lastCalledType);
//                                    switch (dircode) {
//                                        case CallLog.Calls.OUTGOING_TYPE:
//                                            dir = "OUTGOING";
//                                            break;
//                                        case CallLog.Calls.INCOMING_TYPE:
//                                            dir = "INCOMING";
//                                            break;
//
//                                        case CallLog.Calls.MISSED_TYPE:
//                                            dir = "MISSED";
//                                            break;
//                                    }
                                    }

                                }
                                mcursor.close();

//                                Log.e("TAG", "CALL_STATE_IDLE==>" + lastCalledDuration);
//                                Log.e("TAG", "CALL_STATE_IDLE==>" + lastCalledType);

//                            if (lastCalledNumber != null) {

                                if(turnreport==1) {
                                if (lastCalledNumberList.size() != 0) {
                                    SharedPreferences settings = context.getSharedPreferences("Supporter", 0);
                                    studentMobile = settings.getString("stdMobile","");
                                    stdName = settings.getString("stdName","");

                                    String lastCalledNumber1 = lastCalledNumberList.get(0);
                                    String lastCalledNumber2 = lastCalledNumberList.get(lastCalledNumberList.size() - 1);
                                    Log.e("first", lastCalledNumber1);
                                    Log.e("last", lastCalledNumber2);
                                    Log.e("studentmobile",studentMobile+"--");
                                    Log.e("stdName",stdName+"--");

                                    if (lastCalledNumber2.equals(studentMobile)) {
                                        lastCalledNumber = lastCalledNumber2;
                                        lastCalledType = lastCalledTypeList.get(lastCalledNumberList.size() - 1);
                                        lastCalledDuration = lastCalledDurationList.get(lastCalledNumberList.size() - 1);
                                    }
                                    if (lastCalledNumber1.equals(studentMobile)) {
                                        lastCalledNumber = lastCalledNumber1;
                                        lastCalledType = lastCalledTypeList.get(0);
                                        lastCalledDuration = lastCalledDurationList.get(0);
                                    }
                                    if (lastCalledNumber!=null && lastCalledNumber.equals(studentMobile)) {
                                        duration = Integer.parseInt(lastCalledDuration);
                                        if (j == 1) {
                                            Log.e("TAG", "CALL_STATE_IDLE==>" + lastCalledNumber);
                                            Log.e("duration", duration + "--");
                                            String duration1 = String.valueOf(duration);
                                            Intent intent = new Intent(context, FeedbackActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            intent.putExtra("studentName", stdName);
                                            intent.putExtra("reqId", reqId1);
                                            intent.putExtra("supId", supId);
                                            intent.putExtra("duration", duration1);
                                            context.startActivity(intent);
//                                            ReportCall(reqId1, duration1);
                                            j++;
                                        }
                                    } else {
                                        Log.e("number call end", "is not equal to supporter");
                                    }
                                }
                                turnreport++;
                            }

                            }
                        }, 1000);


                }
                if ((prev_state == TelephonyManager.CALL_STATE_RINGING)) {
                    prev_state = state;
                    //Rejected or Missed call
                }

            }
        }
    }






    public class ViewHolder {
        TextView studentName, studentGrade, slotTime;
        LinearLayout call;
    }

    private void CheckRequestValidAndCall(final String reqId, int position) {
        final int position1 = position;
        SpotsDialog alertDialog = new SpotsDialog(context, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        Map<String, String> CheckRequestValidParams = new HashMap<String, String>();
        CheckRequestValidParams.put("reqId", reqId);
        final String CheckRequestValidUrl = context.getResources().getString(R.string.url) + "CheckRequestValid";
        new VolleyPost(context, CheckRequestValidUrl, CheckRequestValidParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("CheckRequestValid: ", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    String success = jsonObject.getString("success");
                    if (success.equals("1")) {

                        setReqForSupporter(supId, reqId, position1);

                    } else if(success.equals("0")) {

                        Toast.makeText(context, "به این درخواست پاسخ داده شده است. لیست درخواست ها را مجددا بارگذاری کنید!", Toast.LENGTH_LONG).show();

                    } else if(success.equals("2")) {

                        Toast.makeText(context, "لطفا در زمان تعیین شده تماس بگیرید!", Toast.LENGTH_LONG).show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void setReqForSupporter(String supporterId, String reqId, int position) {
        final int position1 = position;
        SpotsDialog alertDialog = new SpotsDialog(context, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        Map<String, String> setsupParams = new HashMap<String, String>();
        setsupParams.put("supporterId", supporterId);
        setsupParams.put("reqId", reqId);
        final String setsupUrl = context.getResources().getString(R.string.url) + "setReqForSupporter";
        new VolleyPost(context, setsupUrl, setsupParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("setReqForSupporter: ", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    String success = jsonObject.getString("success");
                    if (success.equals("1")) {
//                        askForPermision(Manifest.permission.CALL_PHONE, CALL, position1);
                        SharedPreferences settings = context.getSharedPreferences("Supporter", 0);
                        studentMobile = settings.getString("stdMobile","");
                        stdName = settings.getString("stdName","");
                        stdGrade = settings.getString("stdGrade","");
                        SmsToStudent(supId,studentMobile);
                        Alertdialog_request(stdName,stdGrade,studentMobile,position1);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @SuppressLint("MissingPermission")
    private void askForPermision(String permision, Integer requestCode, String studentMobile0) {
//        if (ActivityCompat.checkSelfPermission(context, permision) != PackageManager.PERMISSION_GRANTED) {
//            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, permision)) {
//                ActivityCompat.requestPermissions((Activity) context, new String[]{permision}, requestCode);
//            } else {
//                ActivityCompat.requestPermissions((Activity) context, new String[]{permision}, requestCode);
//            }
//        } else {
//            if (requestCode.equals(CALL)) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + studentMobile0));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
//            }
//        }
    }

//    private void ReportCall(String reqId0, final String duration) {
//        SpotsDialog alertDialog = new SpotsDialog(context, R.style.Custom);
//        alertDialog.setCancelable(false);
//        alertDialog.setCanceledOnTouchOutside(false);
//
//        Map<String, String> ReportCallParams = new HashMap<String, String>();
//        ReportCallParams.put("reqId", reqId0);
//        ReportCallParams.put("duration", duration);
//        final String ReportCallUrl = context.getResources().getString(R.string.url) + "ReportCall";
//        new VolleyPost(context, ReportCallUrl, ReportCallParams, 1000, alertDialog, new VolleyPost.onResponse() {
//            @Override
//            public void onFinish(String Result) {
//                try {
//                    Log.e("ReportCall: ", Result);
//                    JSONObject jsonObject = new JSONObject(Result);
//                    String success = jsonObject.getString("success");
//
//                    int Dur = Integer.parseInt(duration);
//                    if (success.equals("1")) {
////                        if (Dur < 40) {
////                            Toast.makeText(context, "تماس شما از ۴۰ ثانیه کمتر بوده و ناموفق است!", Toast.LENGTH_LONG).show();
////                        } else if (Dur >= 40) {
////                            Intent intent = new Intent(context, FeedbackActivity.class);
////                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
////                            intent.putExtra("studentName", stdName);
////                            intent.putExtra("reqId", reqId1);
////                            intent.putExtra("supId", supId);
////                            context.startActivity(intent);
////                        }
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//    }


    public void Alertdialog_request(String studentName0, String studentGrade0, final String studentMobile0, int position0) {

        final int position1 = position0;
        final Dialog My_alertdialog = new Dialog(context);
        My_alertdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        My_alertdialog.setContentView(R.layout.alert_request);
        final ProgressBar progressBar = My_alertdialog.findViewById(R.id.progress_waitForAcceptReq);
        final ImageView callImage = My_alertdialog.findViewById(R.id.callimage);
        final TextView callText = My_alertdialog.findViewById(R.id.calltext);

        callImage.setImageDrawable(context.getResources().getDrawable(R.drawable.gcall1));
        callText.setTextColor(context.getResources().getColor(R.color.gray));


        TextView stdName = My_alertdialog.findViewById(R.id.tv_studentNameAndGrand);
        stdName.setText(" "+studentName0+" - "+studentGrade0);

        RelativeLayout callReq = My_alertdialog.findViewById(R.id.call_layout);
        callReq.setEnabled(true);
        final CountDownTimer countDownTimer = new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                turn0 = 0;
                callImage.setImageDrawable(context.getResources().getDrawable(R.drawable.gcall1));
                callText.setTextColor(context.getResources().getColor(R.color.gray));
                progressBar.setProgress(60-(int) (millisUntilFinished/1000));
            }

            public void onFinish() {
                turn0 = 1;
                callImage.setImageDrawable(context.getResources().getDrawable(R.drawable.call1));
                callText.setTextColor(context.getResources().getColor(R.color.green));

                progressBar.setProgress(60);
                progressBar.clearAnimation();

            }
        };
        countDownTimer.start();

        callReq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(turn0==0){
                    Toast.makeText(context,"امکان برقرای تماس تا چند ثانیه دیگر فراهم خواهد شد.",Toast.LENGTH_LONG).show();
                }else if(turn0==1){
                    askForPermision(Manifest.permission.CALL_PHONE, CALL, studentMobile0);
                    My_alertdialog.cancel();
                }

            }
        });

        My_alertdialog.setCancelable(false);
        My_alertdialog.show();



    }

    public void SmsToStudent(String supId,String studentMobile){

        SpotsDialog alertDialog = new SpotsDialog(context, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        Map<String, String> SmsToStudentParams = new HashMap<String, String>();
        SmsToStudentParams.put("supId", supId);
        SmsToStudentParams.put("userMobile", studentMobile);
        final String SmsToStudentUrl = context.getResources().getString(R.string.url) + "SmsToStudent";
        new VolleyPost(context, SmsToStudentUrl, SmsToStudentParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("SmsToStudent: ", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    String success = jsonObject.getString("success");
                    Log.e("SmsToStudentSuccess","--"+success);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
