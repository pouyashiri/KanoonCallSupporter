package com.example.rekabi.counsellorkanoon;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.crashlytics.android.Crashlytics;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HomeActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    private String supId;
    private SpotsDialog alertDialog;
    private LinearLayout requestList;
    private Button LogOut,reloadRequest;
    private ListView listView;
    private TextView score_number;
    private RatingBar ratingBar;
    private ArrayList<String> reqId = new ArrayList<>();
    private ArrayList<String> slotTime = new ArrayList<>();
    private ArrayList<String> studentName = new ArrayList<>();
    private ArrayList<String> studentPhone = new ArrayList<>();
    private ArrayList<String> studentGrade = new ArrayList<>();
    private static final String TAG = "PhoneStateBroadcastReceiver";
    private ToggleButton online_switch;
    private int isOnline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getSupportActionBar().hide();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Fabric.with(this, new Crashlytics());
        logUser();
        Log.e("onCreate----------","-------------im here");
        score_number = findViewById(R.id.score_number);
        ratingBar = findViewById(R.id.supScore);
        requestList = findViewById(R.id.requestList);
        listView = findViewById(R.id.RequestsList);
        reloadRequest = findViewById(R.id.reloadRequest);
        LogOut = findViewById(R.id.LogOut);
        requestList.setVisibility(View.INVISIBLE);
        listView.setVisibility(View.INVISIBLE);
        reloadRequest.setVisibility(View.INVISIBLE);
        online_switch = findViewById(R.id.online_switch);


        Log.e("Oncreate----------","-------------im here");
        SharedPreferences preferences = this.getSharedPreferences("Supporter", 0);
        if (preferences.contains("supId")) {
            supId = preferences.getString("supId", "");
        }


        String Online = getIntent().getStringExtra("isOnline");
        if(Online==null){
            SharedPreferences preference = this.getSharedPreferences("Supporter", 0);
            if(preference.contains("isOnline")){
                Online = preference.getString("isOnline", "");
                Log.e("Online----------","-----"+Online);
            }else{
                Online = "0";
            }
        }
        Log.e("Online----------","-----"+Online);
        if(Online!=null){
            if(Online.equals("0")){
                online_switch.setChecked(false);
                SetAvailable(supId,"0");
            }else if(Online.equals("1")){
                online_switch.setChecked(true);
                SetAvailable(supId,"1");
                GetRequestsList();
                requestList.setVisibility(View.VISIBLE);
                listView.setVisibility(View.VISIBLE);
                reloadRequest.setVisibility(View.VISIBLE);
            }
        }


        online_switch.setOnCheckedChangeListener(this);
    }

    @Override
    protected void onResume() {

        Log.e("onResume----------","-------------im here");
        ratingBar.setRating(0);
        score_number.setText(" "+"۰"+" نفر ");

        LogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Alertdialog_exit();
            }
        });

        SharedPreferences preferences = this.getSharedPreferences("Supporter", MODE_PRIVATE);
        if(preferences.contains("feedbackDone")&&!preferences.getString("feedbackDone","").equals("3")){
            if(preferences.getString("feedbackDone","").equals("0")){
                Intent intent = new Intent(HomeActivity.this, FeedbackActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                String stdname = preferences.getString("studentName","");
                String mreqId = preferences.getString("reqId","");
                intent.putExtra("reqId", mreqId);
                intent.putExtra("studentName",stdname);
                startActivity(intent);
            }else if(preferences.getString("feedbackDone","").equals("1")){
                Intent intent = new Intent(HomeActivity.this, CreateBookRecomActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                String stdname = preferences.getString("studentName","");
                String mreqId = preferences.getString("reqId","");
                intent.putExtra("reqId", mreqId);
                intent.putExtra("studentName",stdname);
                startActivity(intent);
            }else if(preferences.getString("feedbackDone","").equals("2")){
                Intent intent = new Intent(HomeActivity.this, FinalBookRecomActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                String stdname = preferences.getString("studentName","");
                String mreqId = preferences.getString("reqId","");
                intent.putExtra("reqId", mreqId);
                intent.putExtra("studentName",stdname);
                startActivity(intent);
            }
        }else {
            Bundle data = getIntent().getExtras();
            if (data != null) {
                supId = data.getString("supId");
            }

            if(supId==null){
                if (preferences.contains("supId")) {
                    supId = preferences.getString("supId", "");
                }
            }



            reloadRequest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    requestList.setVisibility(View.INVISIBLE);
                    listView.setVisibility(View.INVISIBLE);
                    reloadRequest.setVisibility(View.INVISIBLE);
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            GetRequestsList();
                        }
                    }, 10);

                }
            });
        }
//        Log.e("onResume-------","-----------------im here");
        SupporterAvrgScore();

        super.onResume();
    }


    public void GetRequestsList() {



        alertDialog = new SpotsDialog(HomeActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        Map<String, String> RequestsParams = new HashMap<String, String>();
        RequestsParams.put("supporterId", supId);
        final String GetCurrentRequestsUrl = getResources().getString(R.string.url)+"GetCurrentRequests";
        new VolleyPost(HomeActivity.this, GetCurrentRequestsUrl, RequestsParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    reqId.clear();
                    slotTime.clear();
                    studentName.clear();
                    studentPhone.clear();
                    studentGrade.clear();
                    Log.e("GetCurrentRequests: ", Result);
                    JSONArray jsonArrayResult = new JSONArray(Result);
                    for (int i = 0; i < jsonArrayResult.length(); i++) {
                        JSONObject jsonObject2 = jsonArrayResult.getJSONObject(i);
                        reqId.add(jsonObject2.getString("reqId"));
                        slotTime.add(jsonObject2.getString("reserveDate"));
                        studentName.add(jsonObject2.getString("usrName"));
                        studentPhone.add(jsonObject2.getString("usrMobile"));
                        studentGrade.add(jsonObject2.getString("usrGroup"));
                    }
                    RequestListAdapter requestListAdapter = new RequestListAdapter(HomeActivity.this, reqId, studentName, studentPhone, studentGrade, slotTime, supId);
                    listView.setAdapter(requestListAdapter);
                    requestList.setVisibility(View.VISIBLE);
                    listView.setVisibility(View.VISIBLE);
                    reloadRequest.setVisibility(View.VISIBLE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void SupporterAvrgScore(){
        alertDialog = new SpotsDialog(HomeActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        Map<String, String> AvrgScoreParams = new HashMap<String, String>();
        AvrgScoreParams.put("supporterId", supId);
        final String AvrgScoreUrl = getResources().getString(R.string.url)+"SupporterAvrgScore";
        new VolleyPost(HomeActivity.this, AvrgScoreUrl, AvrgScoreParams, 1000, null, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("SupporterAvrgScore: ", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    String success = jsonObject.getString("success");
                    if(success.equals("1")){
                        float score = jsonObject.getInt("score");
                        int number = jsonObject.getInt("number");
                        ratingBar.setRating(score);
                        score_number.setText(" "+number+" نفر ");
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    public void Alertdialog_exit() {
        final Dialog My_alertdialog = new Dialog(HomeActivity.this);
        My_alertdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        My_alertdialog.setContentView(R.layout.alert_style_exit);
        Button okRequest = (Button) My_alertdialog.findViewById(R.id.yesExit);
        okRequest.setEnabled(true);
        Button noRequest = (Button) My_alertdialog.findViewById(R.id.noExit);
        noRequest.setEnabled(true);

        okRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String state = "0";
                SetAvailable(supId, state);

                SharedPreferences settings = getSharedPreferences("Supporter", MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.clear();
                editor.commit();
                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                My_alertdialog.cancel();
            }
        });
        noRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                My_alertdialog.cancel();
            }
        });
        My_alertdialog.setCancelable(true);
        My_alertdialog.show();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onCheckedChanged(CompoundButton compound, boolean isChecked) {
        String state;
        if (isChecked) {
            state = "1";
            isOnline=1;
            SetAvailable(supId, state);
            GetRequestsList();
            requestList.setVisibility(View.VISIBLE);
            listView.setVisibility(View.VISIBLE);
            reloadRequest.setVisibility(View.VISIBLE);
//            RequestListAdapter requestListAdapter = new RequestListAdapter(HomeActivity.this,reqId,studentName,studentPhone,studentGrade,slotTime,supId);
//            listView.setAdapter(requestListAdapter);

        } else {
            state = "0";
            isOnline=0;
            SetAvailable(supId, state);
            requestList.setVisibility(View.INVISIBLE);
            listView.setVisibility(View.INVISIBLE);
            reloadRequest.setVisibility(View.INVISIBLE);
        }

    }

    public void SetAvailable(String supId,String state) {
        if(state.equals("0")){
            isOnline=0;
            requestList.setVisibility(View.INVISIBLE);
            listView.setVisibility(View.INVISIBLE);
            reloadRequest.setVisibility(View.INVISIBLE);
        }else if(state.equals("1")){
            isOnline=1;
            requestList.setVisibility(View.VISIBLE);
            listView.setVisibility(View.VISIBLE);
            reloadRequest.setVisibility(View.VISIBLE);
        }
        SharedPreferences settings = getSharedPreferences("Supporter", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("isOnline", String.valueOf(isOnline));
        editor.commit();
        editor.apply();

        alertDialog = new SpotsDialog(HomeActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        Map<String, String> setStateParams = new HashMap<String, String>();
        setStateParams.put("supporterId", supId);
        setStateParams.put("state", state);
        final String SetAvailableUrl = getResources().getString(R.string.url)+"SetAvailable";
        new VolleyPost(HomeActivity.this, SetAvailableUrl, setStateParams, 1000, null, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("SetAvailable: ", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    String login_success = jsonObject.getString("success");
                    Log.e("login_success + :::", login_success);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    @Override
    protected void onDestroy() {
        Log.e("onDestroy-----------","------------im here");
        if (alertDialog!=null) {
            alertDialog.dismiss();
            alertDialog=null;
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("EXIT", true);
        startActivity(intent);
    }



    @Override
    protected void onStop() {
        super.onStop();
        Log.e("onStop-----","--------------im here");
        Log.e("isOnline-----","---"+isOnline);
        SharedPreferences settings = getSharedPreferences("Supporter", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("isOnline", String.valueOf(isOnline));
        editor.commit();
        editor.apply();

//        online_switch.setChecked(false);
//        SetAvailable(supId,"0");

    }



    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e("onrestart----------","-------------im here");
        SharedPreferences preferences = this.getSharedPreferences("Supporter", MODE_PRIVATE);
        String Online = preferences.getString("isOnline", "");
        Log.e("Online----------","-----"+Online);
        if(Online.equals("0")){
            online_switch.setChecked(false);
            SetAvailable(supId,"0");
        }else if(Online.equals("1")){
            online_switch.setChecked(true);
            SetAvailable(supId,"1");
            GetRequestsList();
            requestList.setVisibility(View.VISIBLE);
            listView.setVisibility(View.VISIBLE);
            reloadRequest.setVisibility(View.VISIBLE);
        }
    }


    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        SharedPreferences settings = getSharedPreferences("Supporter", HomeActivity.this.MODE_PRIVATE);
        Crashlytics.setUserIdentifier(settings.getString("supId", ""));
    }

}
