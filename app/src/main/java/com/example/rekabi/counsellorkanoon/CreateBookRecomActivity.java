package com.example.rekabi.counsellorkanoon;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CreateBookRecomActivity extends AppCompatActivity {
    int turn0 = 1;
    private TextView tv_studentName;
    private SpotsDialog alertDialog;
    private ArrayList<String> stageCode = new ArrayList<>();
    private ArrayList<String> stageName = new ArrayList<>();
    private ArrayList<String> groupCode = new ArrayList<>();
    private ArrayList<String> groupName = new ArrayList<>();
    private ArrayList<String> courseCode = new ArrayList<>();
    private ArrayList<String> courseName = new ArrayList<>();
    private ArrayList<Integer> addCheckBoxValue = new ArrayList<>();

    private ArrayList<String> BookName = new ArrayList<>();
    private ArrayList<String> BookPrice = new ArrayList<>();
    private ArrayList<String> BookId = new ArrayList<>();
    private String reqId, courseId, groupId;
    private Button setRecom, searchBook;
    private ListView listView;
    private GetBookListAdapter getBookListAdapter;
    private String studentName,supId;
    private String values,feedbackDone;
    private  Spinner sp_stage, sp_group, sp_course;
    private ArrayList<String> AllBookName = new ArrayList<>();
    private ArrayList<String> AllBookPrice = new ArrayList<>();
    private ArrayList<String> setBookName = new ArrayList<>();
    private ArrayList<String> setBookPrice = new ArrayList<>();
    private ArrayList<Integer> setBookId = new ArrayList<>();
    private ArrayList<Integer> setRecomId = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_book_recom);
        getSupportActionBar().hide();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Fabric.with(this, new Crashlytics());
        logUser();


        sp_stage = findViewById(R.id.sp_stage);
        sp_group = findViewById(R.id.sp_group);
        sp_course = findViewById(R.id.sp_course);
        searchBook = findViewById(R.id.bt_searchBook);
        setRecom = findViewById(R.id.bt_setRecom);
        listView = findViewById(R.id.bookList);
        tv_studentName = findViewById(R.id.tv_studentName);

        reqId = getIntent().getStringExtra("reqId");
        studentName = getIntent().getStringExtra("studentName");
        supId = getIntent().getStringExtra("supId");
        tv_studentName.setText(studentName);

        SharedPreferences settings = getSharedPreferences("Supporter", 0);
        SharedPreferences.Editor editor = settings.edit();

        if(supId==null){
            supId = settings.getString("supId","");
            reqId = settings.getString("reqId", "");
            studentName = settings.getString("studentName", "");
            tv_studentName.setText(studentName);
        }


        feedbackDone = "1";
        editor.putString("feedbackDone", feedbackDone);
        editor.putString("reqId", reqId);
        editor.putString("supId", supId);
        editor.putString("studentName", studentName);
        editor.commit();
        editor.apply();


        GetStages();



        sp_stage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                searchBook.setEnabled(true);
                if (position >-1) {

                    String stageId = stageCode.get(position);
                    GetGroups(stageId);


                    courseCode.clear();
                    courseName.clear();
//                    courseName.add("درس مورد نظر خود را انتخاب نمایید");
//                    courseCode.add("0");
                    ArrayAdapter<String> courseAdapter = new ArrayAdapter<String>(CreateBookRecomActivity.this, R.layout.spinner_item, courseName);
                    courseAdapter.setDropDownViewResource(R.layout.spinner_item);
                    sp_course.setAdapter(courseAdapter);

//                    listView.setVisibility(View.INVISIBLE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_group.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                searchBook.setEnabled(true);
                if (position >-1) {
                    groupId = groupCode.get(position);
                    GetCourses(groupId);

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                groupId = groupCode.get(0);
                GetCourses(groupId);
            }
        });

        sp_course.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != 0) {
                    courseId = courseCode.get(position);
                    searchBook.setEnabled(true);
                    GetBooks(courseId,groupId);
//                    listView.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                courseId = courseCode.get(0);
                searchBook.setEnabled(true);
                GetBooks(courseId,groupId);
            }
        });

        searchBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                AllBookName.clear();
//                AllBookPrice.clear();
//                addCheckBoxValue.clear();
                if(BookName.size()==0){
                    Toast.makeText(CreateBookRecomActivity.this,"کتابی یافت نشد!",Toast.LENGTH_LONG).show();
                }else{
                    SharedPreferences settings = getSharedPreferences("Supporter", 0);
                    if (!settings.contains("SetBookName")) {
                        addCheckBoxValue.clear();
                        for(int i=0;i<BookName.size();i++) {
                            addCheckBoxValue.add(0);
                        }
                        GetBookListAdapter getBookListAdapter = new GetBookListAdapter(CreateBookRecomActivity.this, reqId,BookId, BookName, BookPrice, addCheckBoxValue);
                        listView.setAdapter(getBookListAdapter);

                    }else{
                        settings = getSharedPreferences("Supporter", 0);
                        addCheckBoxValue.clear();
                        ArrayList<String> sBookName = new ArrayList<>();
                        ArrayList<String> sBookPrice = new ArrayList<>();
                        ArrayList<String> sBookId = new ArrayList<>();

                            try {
                                JSONArray bookNameA = new JSONArray(settings.getString("SetBookName",""));
                                JSONArray bookPriceA = new JSONArray(settings.getString("SetBookPrice",""));
                                JSONArray bookIdA = new JSONArray(settings.getString("SetBookId",""));

                                for (int i =0;i<bookNameA.length();i++){
                                    sBookName.add(bookNameA.getString(i));
                                    sBookPrice.add(bookPriceA.getString(i));
                                    sBookId.add(bookIdA.getString(i));
                                    addCheckBoxValue.add(1);
                                }
                                for(int position=0;position<BookName.size();position++) {
                                    if(!sBookName.contains(BookName.get(position))){
                                        sBookName.add(BookName.get(position));
                                        sBookPrice.add(BookPrice.get(position));
                                        sBookId.add(BookId.get(position));
                                        addCheckBoxValue.add(0);
                                    }
                                }

                                GetBookListAdapter getBookListAdapter = new GetBookListAdapter(CreateBookRecomActivity.this, reqId, sBookId,sBookName, sBookPrice, addCheckBoxValue);
                                listView.setAdapter(getBookListAdapter);
                                listView.setSelection(bookNameA.length());

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                    }
                }

//
//                int s = getBookListAdapter.getCheckBoxValue().size();
////
//
//                if(getBookListAdapter.getCheckBoxValue().size()!=0){
//                    ArrayList<Integer> Values =getBookListAdapter.getCheckBoxValue();
//                    for (int i = 0; i < Values.size(); i++) {
//                        if (Values.get(i)==1) {
//                            setBookName.add(BookName.get(i));
//                            setBookPrice.add(BookPrice.get(i));
//                        }
//                    }
//                }
//
//                listView.setVisibility(View.VISIBLE);
//                if(BookName.size()==0){
//                    Toast.makeText(getApplicationContext(),"کتابی یافت نشد.",Toast.LENGTH_LONG).show();
//                    for(int j=0;j<setBookName.size();j++){
//                        AllBookName .add( setBookName.get(j));
//                        AllBookPrice.add(setBookPrice.get(j));
//                    }

//                    if(AllBookName.size()!=0){
//                        for (int i = 0; i < AllBookName.size(); i++) {
//                            addCheckBoxValue.add(1);
//                        }
//                        getBookListAdapter = new GetBookListAdapter(CreateBookRecomActivity.this, reqId, AllBookName, AllBookPrice, addCheckBoxValue);
//                        listView.setAdapter(getBookListAdapter);
//                    }
//                }
//
//                else{
//                    for(int j=0;j<setBookName.size();j++){
//                        AllBookName .add( setBookName.get(j));
//                        AllBookPrice.add(setBookPrice.get(j));
//                    }
//                    if(AllBookName.size()!=0){
//                        for (int i = 0; i < AllBookName.size(); i++) {
//                            addCheckBoxValue.add(1);
//                        }
//                        for (int i = 0; i < BookName.size(); i++) {
//                            addCheckBoxValue.add(0);
//                            AllBookName.add(BookName.get(i));
//                            AllBookPrice.add(BookPrice.get(i));
//                        }
//                        getBookListAdapter = new GetBookListAdapter(CreateBookRecomActivity.this, reqId, AllBookName, AllBookPrice, addCheckBoxValue);
//                        listView.setAdapter(getBookListAdapter);
//                    }else{
//                        for (int i = 0; i < BookName.size(); i++) {
//                            addCheckBoxValue.add(0);
//                        }
//                        getBookListAdapter = new GetBookListAdapter(CreateBookRecomActivity.this, reqId, BookName, BookPrice, addCheckBoxValue);
//                        listView.setAdapter(getBookListAdapter);
//                    }


//                }


            }
        });

        setRecom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                ArrayList<Integer> Value = getBookListAdapter.getCheckBoxValue();

                //////// for log /////////
//                JSONArray array = new JSONArray(Value);
//                String values0 = array.toString();
//                values = values0.substring(1, values0.length() - 1);
//                Log.e("Values", values);
                //////////////////////////

                RegisterRecommendation();

            }
        });

    }


    public void GetStages() {
        stageCode.clear();
        stageName.clear();

//        stageName.add("مقطع مورد نظر خود را انتخاب نمایید");
//        stageCode.add("0");


        alertDialog = new SpotsDialog(CreateBookRecomActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        Map<String, String> GetStageParams = new HashMap<String, String>();
        final String GetStageUrl = getResources().getString(R.string.url)+"GetStage";
//        final String GetStageUrl = "http://localhost:2229/OnlineSupport/GetStage";
        new VolleyPost(CreateBookRecomActivity.this, GetStageUrl, GetStageParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("GetStages: ", Result);
                    JSONArray jsonArray = new JSONArray(Result);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject stages = jsonArray.getJSONObject(i);
                        stageCode.add(stages.getString("StageId"));
                        stageName.add(stages.getString("StageName"));
                    }
                    ArrayAdapter<String> stageAdapter = new ArrayAdapter<String>(CreateBookRecomActivity.this, R.layout.spinner_item, stageName);
                    stageAdapter.setDropDownViewResource(R.layout.spinner_item);
                    sp_stage.setAdapter(stageAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void GetGroups(String stageId) {
        groupCode.clear();
        groupName.clear();

//        groupName.add("پایه مورد نظر خود را انتخاب نمایید");
//        groupCode.add("0");


        alertDialog = new SpotsDialog(CreateBookRecomActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        Map<String, String> GetGroupParams = new HashMap<String, String>();
        GetGroupParams.put("stageId", stageId);
        final String GetGroupUrl = getResources().getString(R.string.url)+"GetGroup";


        new VolleyPost(CreateBookRecomActivity.this, GetGroupUrl, GetGroupParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("GetGroup: ", Result);
                    JSONArray jsonArray = new JSONArray(Result);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject groups = jsonArray.getJSONObject(i);
                        groupCode.add(groups.getString("GrpId"));
                        groupName.add(groups.getString("GrpName"));
                    }
                    ArrayAdapter<String> groupAdapter = new ArrayAdapter<String>(CreateBookRecomActivity.this, R.layout.spinner_item, groupName);
                    groupAdapter.setDropDownViewResource(R.layout.spinner_item);
                    sp_group.setAdapter(groupAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void GetCourses(String groupId) {
        courseCode.clear();
        courseName.clear();

//        courseName.add("درس مورد نظر خود را انتخاب نمایید");
//        courseCode.add("0");

        alertDialog = new SpotsDialog(CreateBookRecomActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        Map<String, String> GetCourseParams = new HashMap<String, String>();
        GetCourseParams.put("groupId", groupId);
        final String GetCourseUrl = getResources().getString(R.string.url)+"GetCourse";
        new VolleyPost(CreateBookRecomActivity.this, GetCourseUrl, GetCourseParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("GetCourse: ", Result);
                    JSONArray jsonArray = new JSONArray(Result);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject groups = jsonArray.getJSONObject(i);
                        courseCode.add(groups.getString("CourseId"));
                        courseName.add(groups.getString("CourseName"));
                    }
                    ArrayAdapter<String> courseAdapter = new ArrayAdapter<String>(CreateBookRecomActivity.this, R.layout.spinner_item, courseName);
                    courseAdapter.setDropDownViewResource(R.layout.spinner_item);
                    sp_course.setAdapter(courseAdapter);
                    listView.setVisibility(View.VISIBLE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void GetBooks(String CourseId,String groupId) {
        BookName.clear();
        BookPrice.clear();
        BookId.clear();

        alertDialog = new SpotsDialog(CreateBookRecomActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

        Map<String, String> GetCourseParams = new HashMap<String, String>();
        GetCourseParams.put("CourseId", CourseId);
        GetCourseParams.put("groupId", groupId);
        final String GetCourseUrl = getResources().getString(R.string.url)+"GetBooks";

        new VolleyPost(CreateBookRecomActivity.this, GetCourseUrl, GetCourseParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("GetBooks: ", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    JSONArray books = jsonObject.getJSONArray("books");
                    for (int i = 0; i < books.length(); i++) {
                        JSONObject book0 = books.getJSONObject(i);
                        BookName.add(book0.getString("BookTitle"));
                        BookPrice.add(book0.getString("BookPrice"));
                        BookId.add(book0.getString("BookId"));
                    }
//
//                    if(turn0==1){
//                        getBookListAdapter = new GetBookListAdapter(CreateBookRecomActivity.this, reqId, BookName, BookPrice, addCheckBoxValue);
//                        listView.setAdapter(getBookListAdapter);
//                        turn0++;
//                    }
                    Log.e("names",BookName.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void RegisterRecommendation() {

        alertDialog = new SpotsDialog(CreateBookRecomActivity.this, R.style.Custom);
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);

//        for (int i = 0; i < Values.size(); i++) {
//            if (Values.get(i)==1) {
//                setBookName.add(BookName.get(i));
//                setBookPrice.add(BookPrice.get(i));
//                setBookId.add(Integer.parseInt(BookId.get(i)));
//            }
//        }

//        JSONArray array = new JSONArray(setBookId);
        SharedPreferences settings = getSharedPreferences("Supporter", 0);

        final ArrayList<String> sBookName = new ArrayList<>();
        final ArrayList<String> sBookPrice = new ArrayList<>();
        final ArrayList<String> sBookId = new ArrayList<>();
        JSONArray bookNameA = null;
        try {
            bookNameA = new JSONArray(settings.getString("SetBookName",""));
            JSONArray bookPriceA = new JSONArray(settings.getString("SetBookPrice",""));
            JSONArray bookIdA = new JSONArray(settings.getString("SetBookId",""));
            for (int i =0;i<bookNameA.length();i++){
                sBookName.add(bookNameA.getString(i));
                sBookPrice.add(bookPriceA.getString(i));
                sBookId.add(bookIdA.getString(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ArrayList<Integer> ids = new ArrayList<>();
        for(int i=0;i<sBookId.size();i++){
            int id = Integer.parseInt(sBookId.get(i));
            ids.add(id);
        }
        JSONArray array = new JSONArray(ids);
        String setBookId0 = array.toString();
        String bookIds = setBookId0.substring(1, setBookId0.length() - 1);

        Log.e("bookIds",bookIds+"--");
        Map<String, String> Params = new HashMap<String, String>();
        Params.put("bookIds",bookIds);
        Params.put("reqId",reqId);
        final String Url = getResources().getString(R.string.url)+"RegisterRecommendation";
        new VolleyPost(CreateBookRecomActivity.this, Url, Params, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("RegisterRecom ", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    JSONArray RecomsId = jsonObject.getJSONArray("RecomsId");
                    for(int i=0;i<RecomsId.length();i++){
                        setRecomId.add(RecomsId.getInt(i));
                    }
                    Log.e("setRecomId",setRecomId.toString());
                    SharedPreferences settings = getSharedPreferences("Supporter", 0);
                    SharedPreferences.Editor editor = settings.edit();
                    feedbackDone = "2";
                    editor.putString("feedbackDone", feedbackDone);
                    editor.putString("reqId", reqId);
                    editor.putString("supId", supId);
                    editor.putString("studentName", studentName);
                    editor.commit();
                    editor.apply();
                    Intent intent = new Intent(CreateBookRecomActivity.this, FinalBookRecomActivity.class);
                    intent.putExtra("BookNames",sBookName);
                    intent.putExtra("BookPrices",sBookPrice);
                    intent.putExtra("BookIds",sBookId);
                    intent.putExtra("recomIds",setRecomId);
                    intent.putExtra("studentName",studentName);
                    intent.putExtra("reqId",reqId);
                    intent.putExtra("supId", supId);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });




    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (alertDialog!=null) {
            alertDialog.dismiss();
            alertDialog=null;
        }
    }

    @Override
    public void onBackPressed() {

    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        SharedPreferences settings = getSharedPreferences("Supporter", CreateBookRecomActivity.this.MODE_PRIVATE);
        Crashlytics.setUserIdentifier(settings.getString("supId", ""));
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}


