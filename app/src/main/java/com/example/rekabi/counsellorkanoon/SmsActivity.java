package com.example.rekabi.counsellorkanoon;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SmsActivity extends AppCompatActivity {

    private EditText etcode;
    private TextView milisecText;
    private Button LoginButton, reRequest;
    private ProgressBar progress120;
    private String enterCode, Code, supId, supPhone, supName;
    private String registerCode,success;
    private SpotsDialog alertDialog;
    private SharedPreferences preferences;
    private RelativeLayout prog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);
        getSupportActionBar().hide();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Fabric.with(this, new Crashlytics());
        logUser();

        preferences = this.getSharedPreferences("Supporter", 0);

        progress120 = findViewById(R.id.progress);
        etcode = findViewById(R.id.et_code);
        milisecText = findViewById(R.id.tv_milisec);
        LoginButton = findViewById(R.id.bt_login);
        reRequest = findViewById(R.id.bt_reRequest);
        prog = findViewById(R.id.prog);


        Bundle data = getIntent().getExtras();
        if (data != null) {
            supName = data.getString("supName");
            supId = data.getString("supId");
            supPhone = data.getString("supPhone");
        } else {
            if (preferences.contains("supName")) {
                supName = preferences.getString("userName", "");
                supId = preferences.getString("supId", "");
                supPhone = preferences.getString("supPhone", "");
            }

        }

        // SEND SMS to USER
        sendSMS(supPhone);

        // Set Timer
        final CountDownTimer countDownTimer = new CountDownTimer(120000, 1000) {


            public void onTick(long millisUntilFinished) {
                prog.setVisibility(View.VISIBLE);
                milisecText.setText("" + millisUntilFinished / 1000);
                progress120.setVisibility(View.VISIBLE);
            }

            public void onFinish() {
                reRequest.setVisibility(View.VISIBLE);
                LoginButton.setVisibility(View.INVISIBLE);
                progress120.clearAnimation();
                milisecText.setText("");
                prog.setVisibility(View.INVISIBLE);


            }
        };
        countDownTimer.start();
        reRequest.setVisibility(View.INVISIBLE);
        LoginButton.setVisibility(View.VISIBLE);



        LoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enterCode = etcode.getText().toString();
                Log.e("code man", enterCode);
                Log.e("code asli", Code);

                if (enterCode.equals(Code)) {

                    setValid();

                } else {

                    countDownTimer.cancel();
                    Toast.makeText(SmsActivity.this, "کد وارد شده معتبر نمی باشد.", Toast.LENGTH_LONG).show();
                    LoginButton.setVisibility(View.INVISIBLE);
                    reRequest.setVisibility(View.VISIBLE);
                    progress120.clearAnimation();
                    milisecText.setText("");
                    prog.setVisibility(View.INVISIBLE);
                    etcode.setText("");
                }
            }
        });

        reRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countDownTimer.cancel();
                reRequest.setVisibility(View.INVISIBLE);
                LoginButton.setVisibility(View.VISIBLE);
                progress120.clearAnimation();
                milisecText.setText("");
                prog.setVisibility(View.INVISIBLE);

                alertDialog = new SpotsDialog(SmsActivity.this, R.style.Custom);
                alertDialog.setCancelable(false);
                alertDialog.setCanceledOnTouchOutside(false);
                Map<String, String> requestsmsParams = new HashMap<String, String>();
                requestsmsParams.put("mobile", supPhone);

                final String requestsmsUrl =getResources().getString(R.string.urlcommon) + "RequestSms";
                new VolleyPost(SmsActivity.this, requestsmsUrl, requestsmsParams, 1000, alertDialog, new VolleyPost.onResponse() {
                    @Override
                    public void onFinish(String Result) {
                        try {
                            Log.e("requestSms", Result);
                            JSONObject jsonObject = new JSONObject(Result);
                            registerCode = jsonObject.getString("code");
                            Code = registerCode + "";
                            reRequest.setVisibility(View.INVISIBLE);
                            LoginButton.setVisibility(View.VISIBLE);
                            countDownTimer.start();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });


            }
        });
    }


    public void sendSMS(String phone) {

        Map<String, String> requestsmsParams = new HashMap<String, String>();
        requestsmsParams.put("mobile", phone);

        final String requestsmsUrl = getResources().getString(R.string.urlcommon) + "RequestSms";
        new VolleyPost(SmsActivity.this, requestsmsUrl, requestsmsParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("requestSms", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    registerCode = jsonObject.getString("code");
                    Code = registerCode;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    public void setValid() {

        Map<String, String> validParams = new HashMap<String, String>();
        validParams.put("supporterId", supId);

        final String validUrl = getResources().getString(R.string.url) + "ActivateSupporter";
        new VolleyPost(SmsActivity.this, validUrl, validParams, 1000, alertDialog, new VolleyPost.onResponse() {
            @Override
            public void onFinish(String Result) {
                try {
                    Log.e("set Supporter Valid", Result);
                    JSONObject jsonObject = new JSONObject(Result);
                    success = jsonObject.getString("success");
                    if (success.equals("1")){
                        SharedPreferences settings = getSharedPreferences("Supporter", 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("registerCode", Code);
                        editor.putString("supPhone", supPhone);
                        editor.putString("supId", supId);
                        editor.putString("supName", supName);
                        editor.commit();
                        editor.apply();

                        Intent intent = new Intent(SmsActivity.this, ValidWaitingActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        intent.putExtra("registerCode", Code);
                        intent.putExtra("supPhone", supPhone);
                        intent.putExtra("supId", supId);
                        intent.putExtra("supName", supName);
                        startActivity(intent);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (alertDialog != null) {
            alertDialog.dismiss();
            alertDialog = null;
        }
    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        SharedPreferences settings = getSharedPreferences("Supporter", SmsActivity.this.MODE_PRIVATE);
        Crashlytics.setUserIdentifier(settings.getString("supId", ""));
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
